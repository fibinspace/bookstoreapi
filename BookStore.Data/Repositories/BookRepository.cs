﻿using BookStore.Data.Interfaces;
using BStore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.Data.Repositories
{
    class BookRepository : IBookRepository
    {
        public List<Book> books = new List<Book>()
        {
            new Book { Id = 1, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
            new Book { Id = 2, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
            new Book { Id = 3, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
            new Book { Id = 4, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
            new Book { Id = 5, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
            new Book { Id = 6, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
            new Book { Id = 7, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
            new Book { Id = 8, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false },
            new Book { Id = 9, Title = "C# i net core 2.0", Author = "Ovais Khan", PublicationYear = 2012, CallNumber = "+3378945616", IsAvailable = false }
        };
        public List<Book> GetAllBooks()
        {
            return books;
        }

        public Book GetBook(int id)
        {
            return books.FirstOrDefault(x => x.Id == id);
        }
    }
}
